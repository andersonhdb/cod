import {
  DiceRollerDialogue
} from "./dialogue-diceRoller.js";
import {
  ReloadDialogue
} from "./dialogue-reload.js";
import {
  ImprovisedSpellDialogue
} from "./dialogue-improvisedSpell.js";
import {
  createShortActionMessage
} from "./chat.js";
import {
  SkillEditDialogue
} from "./dialogue-skillEdit.js";
import {
  ProgressDialogue
} from "./dialogue-progress.js";
import "../lib/dragster/dragster.js";
import * as ui from "./ui.js";
import * as templates from "./templates.js";
/**
 * Extend the basic ActorSheet with some very simple modifications
 */
export class MtAActorSheet extends ActorSheet {
  constructor(...args) {
    super(...args);

    this._rollParameters = [];
    this._rollDicePool = [];

    Hooks.on("closeProgressDialogue", (app, ele) => {
      if (app === this._progressDialogue) this._progressDialogue = null;
    });
  }

  /* -------------------------------------------- */

  /**
   * Get the correct HTML template path to use for rendering this particular sheet
   * @type {String}
   */
  get template() {
    if (!game.user.isGM && this.actor.limited) return "systems/mta/templates/actors/limited-sheet.html";
    if (this.actor.data.type === "ephemeral") return "systems/mta/templates/actors/ephemeral-sheet.html";
    return "systems/mta/templates/actors/character.html";
  }

  /* -------------------------------------------- */

  /**
   * Extend and override the default options used by the 5e Actor Sheet
   * @returns {Object}
   */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["mta-sheet", "worldbuilding", "sheet", "actor"],
      width: 1166,
      height: 830,
      dragDrop: [{dragSelector: "tbody .item-row", dropSelector: null}],
      tabs: [{
        navSelector: ".tabs",
        contentSelector: ".sheet-body",
        initial: "attributes"
      }]
    });
  }

  /* -------------------------------------------- */

  /**
   * Prepare data for rendering the Actor sheet
   * The prepared data object contains both the actor data as well as additional sheet options
   */



  getData() {
    // Basic data
    let isOwner = this.actor.isOwner;
    const data = {
      owner: isOwner,
      limited: this.actor.limited,
      options: this.options,
      editable: this.isEditable,
      cssClass: isOwner ? "editable" : "locked",
      config: CONFIG.MTA,
      rollData: this.actor.getRollData.bind(this.actor) // What is this?
    };

    // The Actor's data
    const actorData = this.actor.data.toObject(false);
    data.actor = actorData;
    data.data = actorData.data;

    data.items = actorData.items;

    /* const data = super.getData();
    Object.assign(data.data, data.data.data)
    data.data.data = null; */ // Previous fix

    const inventory = {
      firearm: {
        label: "Firearm",
        items: [],
        dataset: ["MTA.DamageShort", "MTA.Range", "MTA.Cartridge", "MTA.Magazine", "MTA.InitiativeShort", "MTA.Size"]
      },
      melee: {
        label: "Melee",
        items: [],
        dataset: ["MTA.Damage", "MTA.Type", "MTA.Initiative", "MTA.Size"]
      },
      armor: {
        label: "Armor",
        items: [],
        dataset: ["MTA.Rating", "MTA.Defense", "MTA.Speed", "MTA.Coverage"]
      },
      equipment: {
        label: "Equipment",
        items: [],
        dataset: ["MTA.DiceBonus", "MTA.Durability", "MTA.Structure", "MTA.Size"]
      },
      ammo: {
        label: "Ammo",
        items: [],
        dataset: ["MTA.Cartridge", "MTA.Quantity"]
      },
      spell: {
        label: "Spells",
        items: [],
        dataset: ["MTA.Arcanum", "MTA.Level", "MTA.PrimaryFactorShort", "MTA.Withstand", "MTA.RoteSkill"]
      },
      activeSpell: {
        label: "Active Spells",
        items: [],
        dataset: ["MTA.Arcanum", "MTA.Level", "MTA.Potency", "MTA.Duration", "MTA.Scale"]
      },
      merit: {
        label: "Merits",
        items: [],
        dataset: ["MTA.Rating"]
      },
      numen: {
        label: "Numina",
        items: [],
        dataset: ["MTA.Reaching"]
      },
      manifestation: {
        label: "Manifestations",
        items: [],
        dataset: []
      },
      influence: {
        label: "Spheres of Influence",
        items: [],
        dataset: ["MTA.Rating"]
      },
      condition: {
        label: "Conditions",
        items: [],
        dataset: ["MTA.Persistent"]
      },
      tilt: {
        label: "Tilts",
        items: [],
        dataset: ["MTA.Environmental"]
      },
      yantra: {
        label: "Yantras",
        items: [],
        dataset: ["MTA.DiceBonus", "MTA.Type"]
      },
      attainment: {
        label: "Attainments",
        items: [],
        dataset: ["MTA.Arcanum", "MTA.Level", "MTA.Legacy"]
      },
      relationship: {
        label: "Relationships",
        items: [],
        dataset: ["MTA.Impression", "MTA.Doors", "MTA.Penalty"]
      },
      vehicle: {
        items: [],
        dataset: ["MTA.DiceBonus", "MTA.Size", "MTA.Durability", "MTA.Structure", "MTA.Speed"]
      },
      devotion: {
        label: "Devotions",
        items: [],
        dataset: ["MTA.Cost", "MTA.Action"]
      },
      rite: {
        label: "Rites and Miracles",
        items: [],
        dataset: ["MTA.Type", "MTA.RiteTarget", "MTA.Withstand"]
      },
      vinculum: {
        label: "Vinculae",
        items: [],
        dataset: ["MTA.VinculumStage"]
      },
      discipline_power: {
        label: "Discipline Powers",
        items: [],
        dataset: ["MTA.Discipline", "MTA.Level", "MTA.Cost", "MTA.Action"]
      },
      container: {
        label: "Containers",
        items: [],
        dataset: ["MTA.Durability", "MTA.Structure", "MTA.Size"]
      },
      service: {
        label: "Services",
        items: [],
        dataset: ["MTA.DiceBonus", "MTA.Skill"]
      },
      contract: {
        label: "Contracts",
        items: [],
        dataset: ["MTA.Type", "MTA.Cost", "MTA.Action", "MTA.Duration"]
      },
      pledge: {
        label: "Pledges",
        items: [],
        dataset: ["MTA.Type"]
      },
      form: {
        items: [],
        descriptions: [],
        dataset: []
      },
      facet: {
        items: [],
        dataset: ["MTA.Gift","MTA.Level","MTA.Cost", "MTA.Action"]
      },
      werewolf_rite: {
        label: "Rite",
        items: [],
        dataset: ["MTA.Type", "MTA.Level", "MTA.Action"]
      }
    };

    

    if (data.data.characterType === "Changeling") inventory.condition.dataset.push("MTA.Clarity");

    //Localise inventory headers
    Object.values(inventory).forEach(section => section.dataset.forEach((item, i, a ) => a[i] = game.i18n.localize(item)));
    data.inventory = inventory;

    data.items.forEach(item => {
      if (data.inventory[item.type]) {
        if (!data.inventory[item.type].items) {
          data.inventory[item.type].items = [];
        }
        data.inventory[item.type].items.push(item);
      }
    });
    
    //Inventory sorting
    const sortFlags = game.user.data.flags?.mta ? game.user.data.flags.mta[this.actor.id] : undefined;
    if(sortFlags){
      for(let itemType in data.inventory){
        const flag = sortFlags[itemType];
        if(flag && flag.sort === "up"){
          data.inventory[itemType].items.sort((a, b) => a.name.localeCompare(b.name))
        }
        else if(flag && flag.sort === "down"){
          data.inventory[itemType].items.sort((a, b) => b.name.localeCompare(a.name))
        }
        else {
          data.inventory[itemType].items.sort((a, b) => b.sort - a.sort);
        }
      }
    }
    else {
      for(let itemType in data.inventory){
        data.inventory[itemType].items.sort((a, b) => b.sort - a.sort);
        
      }
    }

    if(data.data.characterType === "Werewolf"){
      data.inventory.form.items.forEach(form => {
        const item = this.actor.items.get(form._id);
        
        const chatData = item.getChatData({
          secrets: this.actor.isOwner
        });
        data.inventory.form.descriptions.push(chatData.description);
      });
      data.essence_per_turn = CONFIG.MTA.primalUrge_levels[Math.min(9, Math.max(0, data.data.werewolf_traits.primalUrge - 1))].essence_per_turn;
    }

    data.config = CONFIG.MTA;
    if (data.data.characterType === "Mage") data.mana_per_turn = CONFIG.MTA.gnosis_levels[Math.min(9, Math.max(0, data.data.mage_traits.gnosis - 1))].mana_per_turn;
    else if (data.data.characterType === "Proximi") data.mana_per_turn = 1;
    if (data.data.characterType === "Vampire") data.vitae_per_turn = CONFIG.MTA.bloodPotency_levels[Math.min(10, Math.max(0, data.data.vampire_traits.bloodPotency))].vitae_per_turn;
    if (data.data.characterType === "Changeling") data.glamour_per_turn = CONFIG.MTA.glamour_levels[Math.min(9, Math.max(0, data.data.changeling_traits.wyrd - 1))].glamour_per_turn;
    if (this.actor.data.type !== "ephemeral") {
      data.data.progress = [{
        name: "INITIAL",
        beats: data.data.beats + 5 * data.data.experience,
        arcaneBeats: data.data.arcaneBeats + 5 * data.data.arcaneExperience
      }].concat(data.data.progress);

      let beats = data.data.progress.reduce((acc, cur) => {
        if (cur && cur.beats) return acc + +cur.beats;
        else return acc;
      }, 0);
      data.beats_computed = beats % 5;
      data.experience_computed = Math.floor(beats / 5);
    }


    if (data.data.characterType === "Mage") {
      let arcaneBeats = data.data.progress.reduce((acc, cur) => {
        if (cur && cur.arcaneBeats) return acc + 1 * cur.arcaneBeats;
        else return acc;
      }, 0);
      data.arcaneBeats_computed = arcaneBeats % 5;
      data.arcaneExperience_computed = Math.floor(arcaneBeats / 5);
    }

    if (data.data.characterType === "Mage" || data.data.characterType === "Proximi") {
      data.activeSpells = {
        value: data.inventory.activeSpell.items.reduce((acc, cur) => cur.data.isRelinquishedSafely ? acc + 0 : cur.data.isRelinquished ? acc + 0 : acc + 1, 0),
        max: data.data.characterType === "Mage" ? data.data.mage_traits.gnosis : 1
      };
    }

    if (data.data.characterType === "Proximi") {
      data.blessingLimit = {
        value: data.inventory.spell.items.reduce((acc, cur) => {
          return acc + cur.data.level;
        }, 0),
        max: 30
      };
    }

    if (this.actor.data.type === "ephemeral") {
      if (data.data.rank > 10) data.ephemeralEntityName = "Uber-Entity"
      else if (data.data.rank < 1) data.ephemeralEntityName = "Lesser Entity"
      else data.ephemeralEntityName = CONFIG.MTA.ephemeral_ranks[data.data.rank - 1][data.data.ephemeralType];
    }

    //Get additional attributes & skills from config file
    if (this.actor.data.type === "character") {
      Object.entries(CONFIG.MTA.attributes_physical).forEach(([key,value], index) => {
        if(!data.data.attributes_physical[key]) data.data.attributes_physical[key] = {value: 0};
      });
      Object.entries(CONFIG.MTA.attributes_social).forEach(([key,value], index) => {
        if(!data.data.attributes_social[key]) data.data.attributes_social[key] = {value: 0};
      });
      Object.entries(CONFIG.MTA.attributes_mental).forEach(([key,value], index) => {
        if(!data.data.attributes_mental[key]) data.data.attributes_mental[key] = {value: 0};
      });
      Object.entries(CONFIG.MTA.skills_physical).forEach(([key,value], index) => {
        if(!data.data.skills_physical[key]) data.data.skills_physical[key] = {value: 0};
      });
      Object.entries(CONFIG.MTA.skills_social).forEach(([key,value], index) => {
        if(!data.data.skills_social[key]) data.data.skills_social[key] = {value: 0};
      });
      Object.entries(CONFIG.MTA.skills_mental).forEach(([key,value], index) => {
        if(!data.data.skills_mental[key]) data.data.skills_mental[key] = {value: 0};
      });
      Object.entries(CONFIG.MTA.arcana_gross).forEach(([key,value], index) => {
        if(!data.data.arcana_gross[key]) data.data.arcana_gross[key] = {value: 0};
      });
      Object.entries(CONFIG.MTA.arcana_subtle).forEach(([key,value], index) => {
        if(!data.data.arcana_subtle[key]) data.data.arcana_subtle[key] = {value: 0};
      });
      Object.entries(CONFIG.MTA.disciplines_common).forEach(([key,value], index) => {
        if(!data.data.disciplines_common[key]) data.data.disciplines_common[key] = {value: 0};
      });
      Object.entries(CONFIG.MTA.disciplines_unique).forEach(([key,value], index) => {
        if(!data.data.disciplines_unique[key]) data.data.disciplines_unique[key] = {value: 0};
      });
      Object.entries(CONFIG.MTA.werewolf_renown).forEach(([key,value], index) => {
        if(!data.data.werewolf_renown[key]) data.data.werewolf_renown[key] = {value: 0};
      });
    }
    else if (this.actor.data.type === "ephemeral") {
      Object.entries(CONFIG.MTA.eph_physical).forEach(([key,value], index) => {
        if(!data.data.eph_physical[key]) data.data.eph_physical[key] = {value: 0};
      });
      Object.entries(CONFIG.MTA.eph_social).forEach(([key,value], index) => {
        if(!data.data.eph_social[key]) data.data.eph_social[key] = {value: 0};
      });
      Object.entries(CONFIG.MTA.eph_mental).forEach(([key,value], index) => {
        if(!data.data.eph_mental[key]) data.data.eph_mental[key] = {value: 0};
      });
    }

    console.trace(data);
    return data;
  }

  /* -------------------------------------------- */

  /**
   * Activate event listeners using the prepared sheet HTML
   * @param html {HTML}   The prepared HTML object ready to be rendered into the DOM
   */
  activateListeners(html) {
    super.activateListeners(html);

    //Custom select text boxes
    ui.registerCustomSelectBoxes(html, this);
    
    //Health tracker
    /* this._onBoxChange(html); */
    this._initialiseDotTrackers(html);

    // 
    html.find('.cell.item-name span').click(event => this._onItemSummary(event));

    // Collapse item table
    html.find('.item-table .cell.header.first .collapsible.button').click(event => this._onTableCollapse(event));
    
    // Receive collapsed state from flags
    html.find('.item-table .cell.header.first .collapsible.button').toArray().filter(ele => {
      if(this.actor &&  this.actor.id && game.user.data.flags.mta && game.user.data.flags.mta[this.actor.id] && game.user.data.flags.mta[this.actor.id][$(ele).siblings('.sortable.button')[0].dataset.type] && game.user.data.flags.mta[this.actor.id][$(ele).siblings('.sortable.button')[0].dataset.type].collapsed){
        $(ele).parent().parent().parent().siblings('tbody').hide();
        $(ele).addClass("fa-plus-square");
      }
    });
    
    // Sort item table
    html.find('.item-table .cell.header.first .sortable.button').click(event => this._onTableSort(event));
    
    
    // Receive sort state from flags
    html.find('.item-table .cell.header.first .sortable.button').toArray().filter(ele => {
      if(this.actor &&  this.actor.id && game.user.data.flags.mta && game.user.data.flags.mta[this.actor.id] && game.user.data.flags.mta[this.actor.id][ele.dataset.type]){
        const sort = game.user.data.flags.mta[this.actor.id][ele.dataset.type].sort;
        const et = $(ele).children(".fas");
        if (sort === "up") { // sort A-Z
          et.removeClass("fa-sort");
          et.addClass("fa-sort-up");

        }
        else if (sort === "down") { // sort Z-A
          et.removeClass("fa-sort");
          et.removeClass("fa-sort-up");
          et.addClass("fa-sort-down");

        }
      }
    });

    /* Everything below here is only needed if the sheet is editable */
    if (!this.options.editable) return;

    // Update Inventory Item
    html.find('.item-edit').click(event => {
      const itemId = event.currentTarget.closest(".item-edit").dataset.itemId;
      const item = this.actor.items.get(itemId);
      item.sheet.render(true);
    });

    html.find('.item-delete').click(ev => {
      const itemId = event.currentTarget.closest(".item-delete").dataset.itemId;
      this.actor.deleteEmbeddedDocuments("Item",[itemId]);
    });

    // Item Dragging
    
    let handler = ev => this._handleDragEnter(ev)
    html.find('.item-row').each((i, li) => {
      if (li.classList.contains("header")) return;
      //li.setAttribute("draggable", true);
      new Dragster( li );
      li.addEventListener("dragster:enter", ev => this._handleDragEnter(ev) , false);
      li.addEventListener("dragster:leave", ev => this._handleDragLeave(ev) , false);
    });
    
    // Equip Inventory Item
    html.find('.equipped.checkBox input').click(ev => {
      const itemId = event.currentTarget.closest(".equipped.checkBox input").dataset.itemId;
      const item = this.actor.items.get(itemId);
      let toggle = !item.data.data.equipped;
      const updateData = {
        "data.equipped": toggle
      };
      const updated = item.update(updateData);
    });

    html.find('.spell-rote').click(ev => {
      const itemId = event.currentTarget.closest(".spell-rote").dataset.itemId;
      const item = this.actor.items.get(itemId);
      let toggle = !item.data.data.isRote
      const updateData = {
        "data.isRote": toggle
      };
      const updated = item.update(updateData);
    });

    html.find('.spell-praxis').click(ev => {
      const itemId = event.currentTarget.closest(".spell-praxis").dataset.itemId;
      const item = this.actor.items.get(itemId);
      let toggle = !item.data.data.isPraxis
      const updateData = {
        "data.isPraxis": toggle
      };
      const updated = item.update(updateData);
    });

    html.find('.spell-inured').click(ev => {
      const itemId = event.currentTarget.closest(".spell-inured").dataset.itemId;
      const item = this.actor.items.get(itemId);
      let toggle = !item.data.data.isInured
      const updateData = {
        "data.isInured": toggle
      };
      const updated = item.update(updateData);
    });

    html.find('.favicon').click(ev => {
      const itemId = event.currentTarget.closest(".favicon").dataset.itemId;
      const item = this.actor.items.get(itemId);
      let toggle = !item.data.data.isFavorite
      const updateData = {
        "data.isFavorite": toggle
      };
      const updated = item.update(updateData);
    });

    html.find('.relinquish.unsafe').click(ev => {
      const itemId = event.currentTarget.closest(".relinquish.unsafe").dataset.itemId;
      const item = this.actor.items.get(itemId);
      let isRelinquished = !item.data.data.isRelinquished;
      let isRelinquishedSafely = false;
      const updateData = {
        "data.isRelinquished": isRelinquished,
        "data.isRelinquishedSafely": isRelinquishedSafely
      };
      item.update(updateData);
    });

    html.find('.relinquish.safe').click(ev => {
      const itemId = event.currentTarget.closest(".relinquish.safe").dataset.itemId;
      const item = this.actor.items.get(itemId);
      let isRelinquishedSafely = !item.data.data.isRelinquishedSafely;
      let isRelinquished = false;
      const updateData = {
        "data.isRelinquished": isRelinquished,
        "data.isRelinquishedSafely": isRelinquishedSafely
      };
      item.update(updateData);
    });

    html.find('.item-create').click(this._onItemCreate.bind(this));
    html.find('.discipline-create').click(ev => {
      let ownDisciplines = this.actor.data.data.disciplines_own ? duplicate(this.actor.data.data.disciplines_own) : {};
      let discipline = {
        label: "New Discipline",
        value: 0
      };
      let newKey = Object.keys(ownDisciplines).reduce((acc, ele) => {
        return +ele > acc ? +ele : acc;
      }, 0);
      newKey += 1;
      ownDisciplines[newKey] = discipline;
      let obj = {};
      obj['data.disciplines_own'] = ownDisciplines;
      this.actor.update(obj);
    });

    html.find('.discipline-delete').click(ev => {
      let ownDisciplines = this.actor.data.data.disciplines_own ? duplicate(this.actor.data.data.disciplines_own) : {};
      const discipline_key = event.currentTarget.closest(".discipline-delete").dataset.attribute;

      //delete ownDisciplines[discipline_key];
      ownDisciplines['-=' + discipline_key] = null;
      let obj = {};
      obj['data.disciplines_own'] = ownDisciplines;
      this.actor.update(obj);
    });

    html.find('.discipline-edit').click(ev => {
      const et = $(ev.currentTarget);
      et.siblings(".discipline-name").toggle();
      et.siblings(".attribute-button").toggle();
    });

    $('.discipline-edit').on('keypress', function (e) {
      if(e.which === 13){
        const et = $(ev.currentTarget);
        et.siblings(".discipline-name").toggle();
        et.siblings(".attribute-button").toggle();
      }
});

    // Reload Firearm
    html.find('.item-reload').click(ev => this._onItemReload(ev));

    html.find('.progress').click(async ev => {
      if (this._progressDialogue) this._progressDialogue.bringToTop();
      else this._progressDialogue = await new ProgressDialogue(this.actor).render(true);
    });

    html.find('.item-magValue').mousedown(ev => this._onItemChangeAmmoAmount(ev));

    html.find('.skill-specialty').click(ev => {
      event.preventDefault();
      const skill_key = event.currentTarget.dataset.attribute;
      const skill_type = event.currentTarget.dataset.attributetype;
      new SkillEditDialogue(this.actor, skill_key, skill_type).render(true);
    });


    html.find('.arcana-state').click(ev => { //TODO: Improve function
      const arcanum = event.currentTarget.closest(".arcana-state").dataset.attribute;

      if (Object.keys(this.actor.data.data.arcana_subtle).includes(arcanum)) {
        let updateAttribute = duplicate(this.actor.data.data.arcana_subtle);

        if (this.actor.data.data.arcana_subtle[arcanum].isRuling) {
          updateAttribute[arcanum].isRuling = false;
          updateAttribute[arcanum].isInferior = true;

        } else if (this.actor.data.data.arcana_subtle[arcanum].isInferior) {
          updateAttribute[arcanum].isRuling = false;
          updateAttribute[arcanum].isInferior = false;
        } else {
          updateAttribute[arcanum].isRuling = true;
          updateAttribute[arcanum].isInferior = false;
        }

        let obj = {}
        obj['data.arcana_subtle'] = updateAttribute;
        this.actor.update(obj);
      } else if (Object.keys(this.actor.data.data.arcana_gross).includes(arcanum)) {
        let updateAttribute = duplicate(this.actor.data.data.arcana_gross);

        if (this.actor.data.data.arcana_gross[arcanum].isRuling) {
          updateAttribute[arcanum].isRuling = false;
          updateAttribute[arcanum].isInferior = true;

        } else if (this.actor.data.data.arcana_gross[arcanum].isInferior) {
          updateAttribute[arcanum].isRuling = false;
          updateAttribute[arcanum].isInferior = false;
        } else {
          updateAttribute[arcanum].isRuling = true;
          updateAttribute[arcanum].isInferior = false;
        }

        let obj = {}
        obj['data.arcana_gross'] = updateAttribute;
        this.actor.update(obj);
      }
    });

    html.find('.renown-state').click(ev => { //TODO: Improve function
      const renown = event.currentTarget.closest(".renown-state").dataset.attribute;

      if (Object.keys(this.actor.data.data.werewolf_renown).includes(renown)) {
        let updateAttribute = duplicate(this.actor.data.data.werewolf_renown);

        if (this.actor.data.data.werewolf_renown[renown].isAuspice) {
          updateAttribute[renown].isAuspice = false;
          updateAttribute[renown].isTribe = true;

        } else if (this.actor.data.data.werewolf_renown[renown].isTribe) {
          updateAttribute[renown].isAuspice = false;
          updateAttribute[renown].isTribe = false;
        } else {
          updateAttribute[renown].isAuspice = true;
          updateAttribute[renown].isTribe = false;
        }

        let obj = {}
        obj['data.werewolf_renown'] = updateAttribute;
        this.actor.update(obj);
      }
    });

    // Item Rolling
    html.find('.item-table .item-image').click(event => this._onItemRoll(event));
    
    // Werewolf transform
    html.find('.forms-column .item-image').click(event => this._onWerewolfTransform(event));

    // Calculate Max Health
    html.find('.calculate.health').click(event => {
      this.actor.calculateAndSetMaxHealth();
    });

    html.find('.calculate.resource').click(event => {
      this.actor.calculateAndSetMaxResource();
    });

    html.find('.calculate.clarity').click(event => {
      this.actor.calculateAndSetMaxClarity();
    });

    html.find('.perceptionButton').mousedown(ev => {
      switch (ev.which) {
        case 1:
          this.actor.rollPerception(false, true);
          break;
        case 2:
          break;
        case 3:
          //Quick Roll
          this.actor.rollPerception(true, true);
          break;
      }

    });
    
    html.find('.breakingPointButton').mousedown(ev => {
      switch (ev.which) {
        case 1:
          this.actor.rollBreakingPoint(false, false);
          break;
        case 2:
          break;
        case 3:
          //Quick Roll
          this.actor.rollBreakingPoint(true, false);
          break;
      }
    });
    
    html.find('.dissonanceButton').mousedown(ev => {
      switch (ev.which) {
        case 1:
          this.actor.rollDissonance(false, false);
          break;
        case 2:
          break;
        case 3:
          //Quick Roll
          this.actor.rollDissonance(true, false);
          break;
      }
    });

    //Clicking roll button. TODO: arguably, this could be consolidated with the actor.roll() for custom dice pools.
    html.find('.rollButton').mousedown(ev => {
      function ucfirst(s) {
        return s.charAt(0).toLocaleUpperCase() + s.substring(1);
      }

      let attributeChecks = $(".attribute-check:checked");
      this._rollParameters = attributeChecks.toArray().map(v => v.dataset.category);
      this._rollLabels = attributeChecks.toArray().map(v => v.dataset.attributelabel);
      this._rollDicePool = attributeChecks.toArray().map(v => v.dataset.attributevalue);

      let dicepool = this._rollDicePool.reduce((acc, ele) => +acc + +ele, 0);
      

      for (let i = 0; i < this._rollParameters.length; i++) {
        if (this._rollDicePool[i] < 1) {
          let ele = this._rollParameters[i];
          if (ele === "physical" || ele === "social") {
            this._rollLabels[i] += " [unskilled]";
            dicepool -= 1;
          } else if (ele === "mental") {
            this._rollLabels[i] += " [unskilled]";
            dicepool -= 3;
          }
        }
      }
      let flavor = this._rollLabels.join(' + ');

      switch (ev.which) {
        case 1:
          let diceRoller = new DiceRollerDialogue({dicePool: dicepool, targetNumber: 8, flavor: flavor, addBonusFlavor: true, title: flavor});
          diceRoller.render(true);
          break;
        case 2:
          break;
        case 3:
          //Quick Roll
          DiceRollerDialogue.rollToChat({
            dicePool: dicepool,
            flavor: flavor
          });
          break;
      }

      //Uncheck attributes/skills and reset
      //let attributeChecks = $(".attribute-check:checked");
      attributeChecks.prop("checked", !attributeChecks.prop("checked"));
      this._rollParameters = [];
      this._rollDicePool = [];
    });

    //Clicking spellcasting button
    html.find('.improvisedSpellButton').mousedown(ev => this._onActivateSpell(ev));
  }
  
  /* Handles drag-and-drop visual */
  _handleDragEnter(event){
    let ele = event.target.closest(".item-row");
    if(ele) $(ele).addClass('over')
  }
  
  _handleDragLeave(event) {
    let ele = event.target.closest(".item-row");
    if(ele) $(ele).removeClass('over')
  }

  
  _onDragStart(event) {
    const id = event.target.dataset.itemId;
    const source = this.actor.items.get(id);
    const background = source.img;
    let img = $(event.target).find('.item-image');
    img.css("cssText",'background-image: url(' + background + ') !important');

    event.dataTransfer.setDragImage(img[0], 0, 0);
    return super._onDragStart(event);
  }
  /** @override 
  * Exact copy of the foundry code, except for the !target render,
  * and the sortBefore check.
  */
  _onSortItem(event, itemData) {
    // TODO - for now, don't allow sorting for Token Actor overrides
    //if (this.actor.isToken) return;
      
    // Get the drag source and its siblings
    const source = this.actor.items.get(itemData._id);
    if(!source) return;
    const siblings = this.actor.items.filter(i => {
      return (i.data.type === source.data.type) && (i.data._id !== source.data._id);
    });

    // Get the drop target
    const dropTarget = event.target.closest(".item");
    const targetId = dropTarget ? dropTarget.dataset.itemId : null;
    const target = siblings.find(s => s.data._id === targetId);
      
    if(!target) return this.render();
      
    // Ensure we are only sorting like-types
    if (target && (source.data.type !== target.data.type)) return;
    let sortBefore = source.data.sort > target?.data.sort;

    // Perform the sort
    const sortUpdates = SortingHelpers.performIntegerSort(source, {target: target, siblings: siblings, sortBefore: sortBefore});
    const updateData = sortUpdates.map(u => {
      const update = u.update;
      update._id = u.target.data._id;
      return update;
    });
    // Perform the update
    return this.actor.updateEmbeddedDocuments("Item", updateData);

  }

  async _onActivateSpell(ev, spell) {
    this.actor.castSpell(spell);

  }

/** @override */
async _onDropItem(event, data){
  if(data?.data?.data){
    if(data.data.type ==="spell"){
      data.data.data.isPraxis = false;
      data.data.data.isRote = false;
      data.data.data.isInured = false;
    }
    else if(data.data.data.equipped) data.data.data.equipped = false;
  }
  super._onDropItem(event, data);
}


  async _onItemChangeAmmoAmount(ev) {
    const weaponId = ev.currentTarget.closest(".item-magValue").dataset.itemId;
    const weapon = this.actor.items.get(weaponId);
    const magazine = weapon.data.data.magazine;

    if (magazine) {
      let ammoCount = magazine.data.data.quantity;

      switch (ev.which) {
        case 1:
          ammoCount = Math.min(ammoCount + 1, weapon.data.data.capacity);
          break;
        case 2:
          break;
        case 3:
          ammoCount = Math.max(ammoCount - 1, 1);
          break;
      }

      this.actor.updateOwnedItem({
        _id: weapon.data._id,
        'data.magazine.data.data.quantity': ammoCount
      });
    }
  }

  /**
   * Handle reloading of a firearm from the Actor Sheet
   * @private
   */
  async _onItemReload(ev) {

    const weaponId = event.currentTarget.closest(".item-reload").dataset.itemId;
    const weapon = this.actor.items.get(weaponId);
    const weaponData = weapon.data.data;
    if (weaponData.magazine) { // Eject magazine
      createShortActionMessage("Ejects magazine", this.actor);
      
      ev.target.classList.remove("reloaded");

      //Add ejected ammo back into the inventory
      const ammoData = weaponData.magazine.data;
      delete ammoData._id;
      delete ammoData.effects; // TODO: Remove once foundry bug is fixed (can't find the issue??)
      
      let foundElement = this.actor.items.find(item => (item.data.name === ammoData.name) && (item.data.data.cartridge === ammoData.data.cartridge));

      let updateData = [];
      let a;
      //const index = inventory.findIndex(ele => (ele.data.name === ammoData.name) && (ele.data.cartridge === ammoData.cartridge));
      if (foundElement) { // Add ammo to existing magazine
        updateData.push({
          _id: foundElement.data._id,
          'data.quantity': foundElement.data.data.quantity + ammoData.data.quantity
        }); 
      } 
      else a = await this.actor.createEmbeddedDocuments("Item", [ammoData]); // Add new magazine item

      updateData.push({
        _id: weapon.id,
        'data.magazine': null
      }); // Remove magazine from weapon
      this.actor.updateEmbeddedDocuments("Item", updateData);

    } else {
      //Open reload menu
      let ammoList = new ReloadDialogue(weapon, ev.target);
      ammoList.render(true);
    }
  }

  /**
   * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
   * @param {Event} event   The originating click event
   * @private
   */
  _onItemCreate(event) {
    event.preventDefault();
    const header = event.currentTarget;
    const type = header.dataset.type;
    const itemData = {
      name: `New ${type.capitalize()}`,
      type: type,
      data: duplicate(header.dataset)
    };
    delete itemData.data["type"];
    return this.actor.createEmbeddedDocuments("Item", [itemData]);
  }



  /**
   * Handle rolling of an item from the Actor sheet, obtaining the Item instance and dispatching to it's roll method
   * @private
   */
  _onItemRoll(event) {
    event.preventDefault();
    const itemId = event.currentTarget.closest(".item-name").dataset.itemId;
    const item = this.actor.items.get(itemId);

    // Roll spells through the actor
    if (item.data.type === "spell") return this._onActivateSpell(event, item)
    // Otherwise roll the Item directly
    return item.roll();
  }

/**
   * Handle transformation into a werewolf form.
   * @private
   */
  async _onWerewolfTransform(event) { //TODO: move this to actor.js
    event.preventDefault();
    const itemId = event.currentTarget.dataset.itemId;
    const item = this.actor.items.get(itemId);
    let updates = [];
    const forms = this.getData().inventory.form.items;
    
    forms.forEach(f => {
      updates.push({_id: f._id, 'data.effectsActive': f._id === itemId ? true : false});
    });
    item.roll(); // This is simply to print the item card
    await this.actor.updateEmbeddedDocuments("Item", updates);
    this.actor.calculateAndSetMaxHealth();
  }


  /**
   * Handle collapsing of inventory tables (e.g. firearms, etc.).
   * The collapsed state is stored per user for every actor for every table.
   * @private
   */
  _onTableCollapse(event) {
    const et = $(event.currentTarget);
    if (et.hasClass('fa-plus-square')) { // collapsed
      et.removeClass("fa-plus-square");
      et.addClass("fa-minus-square");
      et.parent().parent().parent().siblings('tbody').show();
      
      // Update user flags, so that collapsed state is saved
      let updateData = {'flags':{'mta':{[this.actor._id]:{[et.siblings('.sortable.button')[0].dataset.type]:{collapsed: false}}}}};
      game.user.update(updateData);
      
    } else { // not collapsed
      et.parent().parent().parent().siblings('tbody').hide();
      et.removeClass("fa-minus-square");
      et.addClass("fa-plus-square");
      
      // Update user flags, so that collapsed state is saved
      let updateData = {'flags':{'mta':{[this.actor._id]:{[et.siblings('.sortable.button')[0].dataset.type]:{collapsed: true}}}}};
      game.user.update(updateData);
    }
  }

    
    async _onTableSort(event) {
      const et = $(event.currentTarget).children(".fas");
      if (et.hasClass('fa-sort')) { // sort A-Z
        et.removeClass("fa-sort");
        et.addClass("fa-sort-up");
        
        // Update user flags, so that sorted state is saved
        let updateData = {'flags':{'mta':{[this.actor.id]:{[event.currentTarget.dataset.type]:{sort: 'up'}}}}};
        await game.user.update(updateData);
      }
      else if (et.hasClass('fa-sort-up')) { // sort Z-A
        et.removeClass("fa-sort-up");
        et.addClass("fa-sort-down");
        
        // Update user flags, so that sorted state is saved
        let updateData = {'flags':{'mta':{[this.actor.id]:{[event.currentTarget.dataset.type]:{sort: 'down'}}}}};
        await game.user.update(updateData);
      }
      else {
        et.removeClass("fa-sort-down"); // unsorted
        et.addClass("fa-sort");
        
        // Update user flags, so that sorted state is saved
        let updateData = {'flags':{'mta':{[this.actor.id]:{[event.currentTarget.dataset.type]:{sort: 'none'}}}}};
        await game.user.update(updateData);
      }
      this.render()
    }


  _onItemSummary(event) {
    event.preventDefault();
    let li = $(event.currentTarget).parents(".item-row"),
      item = this.actor.items.get(li.data("item-id")),
      chatData = item.getChatData({
        secrets: this.actor.owner
      }),
      tb = $(event.currentTarget).parents(".item-table");

    let colSpanMax = [...tb.get(0).rows[0].cells].reduce((a, v) => (v.colSpan) ? a + v.colSpan * 1 : a + 1, 0);

    // Toggle summary
    if (li.hasClass("expanded")) {
      let summary = li.next(".item-summary");
      summary.children().children("div").slideUp(200, () => summary.remove());
    } else {
      let tr = $(`<tr class="item-summary"> <td colspan="${colSpanMax}"> <div> ${chatData.description} </div> </td> </tr>`);
      //let props = $(`<div class="item-properties"></div>`);
      //chatData.properties.forEach(p => props.append(`<span class="tag">${p}</span>`));
      //div.append(props);
      let div = tr.children().children("div");
      div.hide();
      li.after(tr);
      div.slideDown(200);
    }
    li.toggleClass("expanded");
  }

  async _initialiseDotTrackers(html){
    let trackers = html.find('.kMageTracker');
    trackers.toArray().forEach( trackerEle => {
      if( trackerEle.dataset && !trackerEle.dataset.initialised){
        let makeHiddenInput = (name,value,dataType="Number") => {
          let i = document.createElement('input');
          i.type = 'hidden';
          i.name = name;
          i.value = value;
          if(dataType){i.dataset.dtype=dataType;}
          return trackerEle.insertAdjacentElement('afterbegin',i);
        }
        let td = trackerEle.dataset;
        let trackerName = td.name || 'unknowntracker';
        let trackerNameDelimiter = '.';
        let trackerType = (td.type)?td.type:'oneState';
        let stateOrder = (td.states)?td.states.split('/'):['off','on'];
        let stateCount = stateOrder.map( v => (td[v])?td[v]*1:0 ).map( (v,k,a) => ((k>0)?a[0]-v:v) ).map( v => (v < 0)?0:v );
        let stateHighest = stateOrder.length -1; 
        let markingOn = (td.marked !== undefined)?true:false;
        let markedBoxes = null, mbInput = null;
        if(markingOn){
          markedBoxes = [...Array(stateCount[0])].map(v => 0);
          td.marked.split(',').forEach( (v,k) => { if(k < markedBoxes.length && (v*1)){ markedBoxes[k] = 1 } } ) 
          mbInput = makeHiddenInput(trackerName + trackerNameDelimiter + 'marked',markedBoxes.join(','),false);
          trackerEle.insertAdjacentElement('afterbegin',mbInput)
        }
        let renderBox = trackerEle.appendChild( document.createElement('div') );
        trackerEle.dataset.initialised = 'yes';
        let inputs = stateOrder.map( (v,k) => {
          if(k === 0){
            trackerEle.insertAdjacentHTML('afterbegin',`<div class="niceNumber" onpointerdown="let v = event.target.textContent;let i= this.querySelector('input');if(v=='+'){i.value++}if(v=='−'){i.value--};i.dispatchEvent(new Event('input',{'bubbles':true}));">
             <input name="${trackerName + trackerNameDelimiter + v}" type="number" value="${stateCount[k]}">
             <div class="numBtns"><div class="plusBtn">+</div><div class="minusBtn">−</div></div>
            </div>`);
            return trackerEle.firstChild;
          } else {
            return makeHiddenInput(trackerName + trackerNameDelimiter + v, stateCount[0] - stateCount[k]);
          }
        });
        
        let updateDots = (num=0, index=false) => {
          let abNum = Math.abs(num);
          
          // update the stateCount
          // if(index) then fill all dots up to & incl. index with num or empty all dots down to & incl. index if they are <=
          if(num > 0 || num < 0){
            if(index !== false){ 
              stateCount.forEach( (c,s,a) => { if(s<=abNum && s > 0){ a[s] = (num > 0)?index + 1:index; } } );
            } else {
              stateCount = stateCount.map( (c,s,a) => (num > 0 && s == abNum)?c+1:( num < 0 && s<= abNum && s > 0)?c-1:c );
            }
          }
          
           // clamping down values in case they somehow got bugged minimum 0, maximum is the count of state 0
          stateCount = stateCount.map( v => (v <0)?0:(v>stateCount[0])?stateCount[0]:v);
          
          // removing marks if the corresponding box is set to status 0
          if(markingOn){
            markedBoxes = markedBoxes.map( (v,k) => (v && k < stateCount[1])?v:0 ); }
          
          // update inputs
          stateCount.forEach( (c,s) => {
            let nuVal = stateCount[0] - c;
            if(inputs[s].value !== undefined && inputs[s].value != nuVal){   inputs[s].value = nuVal; }
          });
          if(markingOn){
            mbInput.value = markedBoxes.join(',');}
          
          
          // render
          let dots = [...Array(stateCount[0])].map( (v,k) => stateCount.slice(1).reduce( (a,scC,scK) => (scC >=(k+1))?scK+1:a ,0) );
          let r = '<div class="boxes">' + dots.reduce( (a,v,k) => a + `<div data-state="${v}" data-index="${k}"${markingOn&&markedBoxes[k]?' data-marked="true" title="Resistant"':''}></div>`,'') + '</div>';
          if( trackerType == 'health' ){ 
            let dicePenalty = dots.slice(-stateHighest).reduce( (a,v) => (v>0)?a+1:a ,0);
            let penaltyMap = {
              '0' : 'physically stable',
              '1' : 'losing consciousness',
              '2' : 'bleeding out',
              '3' : 'dead'
            };
            r += `<div class="info"><span>You are <b>${penaltyMap[dots[dots.length -1]]}</b>.</span><span>Dice Penalty:  <b>−${dicePenalty}</b></span></div>`;
          }
          renderBox.innerHTML = r;
          
          if(num !== 0){   inputs[1].dispatchEvent( new Event('change',{'bubbles':true}) )   }
        };
        
        // attaching event listeners for left and right clicking the states and chaging the max value input
        trackerEle.addEventListener('pointerdown', (e, t = e.target) => {
          if( t.dataset && t.dataset.state ){
            let s = t.dataset.state*1;
            let index = (trackerType == 'oneState' && t.dataset.index)?t.dataset.index*1:false;
            
            if(e.button === 1 && markingOn && t.dataset.index && t.dataset.index < markedBoxes.length ){
              e.preventDefault();
              markedBoxes[t.dataset.index] = (markedBoxes[t.dataset.index])?0:1;
              updateDots('update');
            }
            else{
              updateDots( (e.button === 2)?-s:(s===stateHighest)?-s:s+1,index);
            }
          }
        });
        trackerEle.addEventListener('contextmenu', (e, t = e.target) => { if(t.dataset.state){  e.preventDefault();  } });
        trackerEle.addEventListener('input', (e, t = e.target) => {
          if(t.type == 'number' && t.name == (trackerName + trackerNameDelimiter +stateOrder[0])){
            stateCount[0] = t.value * 1;
            updateDots('update');
          }
        });
        
        // trigger first render
        updateDots();
      }
    });
  }






  _onBoxChange(html) {
    let parent = html.find('.kMageTracker');
    parent.toArray().forEach(tracker => {
      if (!tracker.dataset.active) {
        let td = tracker.dataset;
        let trackerName = td.name || 'unknowntracker';
        let trackerNameDelimiter = '.';
        let tType = (td.type) ? td.type : 'oneState';
        let stateOrder = (td.states) ? td.states.split('/') : ['off', 'on'];
        let initialStateCount = stateOrder.map(v => (td[v]) ? td[v] * 1 : 0).map((v, k, a) => ((k > 0) ? a[0] - v : v)).map(v => (v < 0) ? 0 : v); //let initialStateCount = stateOrder.map( v => (td[v])?td[v]*1:0 );
        let limit = stateOrder.length - 1;
        let dots = [...Array(initialStateCount[0])].map(v => 0);
        let extraContent = () => '';
        let ex = {};
        tracker.dataset.active = 1;
        let renderBox = tracker.appendChild(document.createElement('div'));
        let inputs = stateOrder.map((v, k) => {
          if (k === 0) {
            tracker.insertAdjacentHTML('afterbegin', `<div class="niceNumber" onpointerdown="let v = event.target.textContent;let i= this.querySelector('input');if(v=='+'){i.value++}if(v=='−'){i.value--};i.dispatchEvent(new Event('input',{'bubbles':true}));">
               <input name="${trackerName + trackerNameDelimiter + v}" type="number" value="${initialStateCount[k]}">
               <div class="numBtns"><div class="plusBtn">+</div><div class="minusBtn">−</div></div>
              </div>`);
            return tracker.firstChild;
          } else {
            let i = document.createElement('input');
            i.type = 'hidden';
            i.dataset.dtype = "Number";
            i.name = trackerName + trackerNameDelimiter + v
            i.value = initialStateCount[0] - initialStateCount[k]; //i.value = initialStateCount[k];
            return tracker.insertAdjacentElement('afterbegin', i);
          }
        });

        initialStateCount.slice(1).forEach((sCount, stateN) => {
          let stateNum = stateN + 1;
            [...Array(Math.min(sCount, dots.length))].forEach((v, k) => dots[k] = stateNum);
        });

        let renderBoxes = () => {
          renderBox.innerHTML =
            '<div class="boxes">' + dots.reduce((a, v, k) => a + `<div data-state="${v}" data-index="${k}"></div>`, '') + '</div>' + extraContent();
        };

        let updateDots = (num = false, index = false) => {
          let abNum = Math.abs(num);
          if (index) {
            if (num > 0) {
              dots = dots.map((dot, i) => (dot < num && i <= index) ? num : dot)
            } else if (num < 0) {
              dots = dots.map((dot, i) => (dot <= abNum && dot > 0 && i >= index) ? 0 : dot)
            }
          } else {
            if (num > 0) {
              let updateIndex = dots.findIndex(v => (v < num));
              if (updateIndex > -1) {
                dots[updateIndex] = num;
              } else {
                if (num < limit) {
                  updateDots(num * 1 + 1); //Never gets called
                }
              }
            } else if (num < 0) {
              //let updateIndex = dots.reduce( (a,v,k) => (v == (num*-1) && v > 0)?k:a, -1 );
              let updateIndex = dots.findIndex(v => (v <= abNum && v > 0));

              if (updateIndex > -1) {
                dots[updateIndex] = 0; // dots.map( (v,k) => (k >= updateIndex)?0:v );
              }
            }
          }
          dots.sort().reverse();

          // Special states
          if (tType == 'health') {
            ex.dicePenalty = -1 * dots.slice(-limit).reduce((a, v) => (v > 0) ? a + 1 : a, 0);
            ex.penaltyState = dots[dots.length - 1];
          }
          
          if (tType == 'clarity') {
            const dmg = dots.filter(v => v === 0);
            ex.dicePenalty = (dmg.length < 3) ? -2 : (dmg.length < 5) ? -1 : 0;
            if(dmg.length === dots.length) ex.dicePenalty += 2;
            if(ex.dicePenalty >= 0) ex.dicePenalty = '+' + ex.dicePenalty;
            if(dmg.length === 0) ex.dicePenalty = '?';
          }

          // update inputs
          stateOrder.forEach((state, stateNum) => {
            let sCount = dots.length - dots.reduce((a, d) => (d >= stateNum) ? ++a : a, 0); //let sCount = dots.reduce((a, d) => (d >= stateNum) ? ++a : a, 0);
            let iName = trackerName + trackerNameDelimiter + state;
            let i = inputs.find(v => (v.name && v.name == iName));
            let ele = inputs.find(v => (v.name && v.name == iName));
            let oldValue = (ele) ? +ele.value * 1 : false;
            if (oldValue !== false && oldValue != sCount) {
              //if(iName === "data.health.bashing") this.actor.update({'data.health.value': this.actor.data.data.health.max - sCount});
              i.value = sCount;
              /**
                            i.dispatchEvent(new Event('input', {
                              'bubbles': true
                            }));**/
            }
          });
          //inputs[0].dispatchEvent(new Event('change',{'bubbles':true}));
          renderBoxes();
          if (num !== false) {
            parent.submit();
          }
        };

        if (tType == 'health' && !(this.actor.data.type === "ephemeral")) {
          ex.dicePenalty = 0;
          ex.penaltyState = 0;
          ex.penaltyMap = {
            '0': 'physically stable',
            '1': 'losing consciousness',
            '2': 'bleeding out',
            '3': 'dead'
          };

          extraContent = () => `<div class="info"><span>You are <b>${ex.penaltyMap[ex.penaltyState]}</b>.</span><span>Dice Penalty:  <b>−${Math.abs(ex.dicePenalty)}</b></span></div>`;
        }
        
        if (tType == 'clarity') {
          ex.dicePenalty = 0;
          ex.penaltyState = 0;
          ex.penaltyMap = {
            '+2': 'lucid',
            '+1': 'rational',
            '+0': 'clear-headed',
            '-1': 'hazy',
            '-2': 'loosing track',
            '?': 'in a coma'
          };

          extraContent = () => `<div class="info"><span>You are <b>${ex.penaltyMap[ex.dicePenalty]}</b>.</span><span>Perception:  <b>${ex.dicePenalty}</b></span></div>`;
        }

        if (this.options.editable) {
          tracker.addEventListener('pointerdown', (e, t = e.target) => {
            if (t.dataset.state) {
              let s = t.dataset.state * 1;
              let n = s;
              let index = false;
              if (e.button === 2) {
                n = -s;
              } else {
                n = s + 1;
              }
              if (tType == 'oneState' && t.dataset.index) {
                index = t.dataset.index;
              }
              updateDots((n > limit) ? -limit : n, index);
              //parent.submit();
            }
          });
          tracker.addEventListener('input', (e, t = e.target) => {
            if (t.type == 'number') {
              let nl = +t.value * 1;
              if (nl < dots.length) {
                dots = dots.slice(0, +t.value * 1);
              } else if (nl > dots.length) {
                dots.push(...[...Array(nl - dots.length)].map(v => 0));
              }

              updateDots(0);
            }
          });
        }
        updateDots();
        tracker.dataset.active = 1;
      }
    });
  }

  /** @override */
  async _updateObject(event, formData) {
    // TODO: This can be removed once 0.7.x is release channel
    if (!formData.data) formData = expandObject(formData);
    if (formData.data?.characterType === "Changeling") {
      //Adjust the number of touchstones on clarity update
      let touchstones = formData.data.touchstones_changeling ? duplicate(formData.data.touchstones_changeling) : {};
      let touchstone_amount = Object.keys(touchstones).length;
      const clarity_max = formData.data.clarity?.max ? formData.data.clarity.max : this.object.data.data.clarity.max;
        
      if (touchstone_amount < clarity_max) {
        while (touchstone_amount < clarity_max) {
          touchstones[touchstone_amount + 1] = "";
          touchstone_amount++;
        }
      } else if (touchstone_amount > clarity_max) {
        while (touchstone_amount > clarity_max) {
          touchstones['-=' + touchstone_amount] = null;
          touchstone_amount -= 1;
        }
      }
        formData.data.touchstones_changeling = touchstones;
    }

    // Update the Item
    await super._updateObject(event, formData);
  }

}